#!/bin/bash


# This script assumes youre working the root of the repository.

for i in "saltstack/salt" "saltstack/pillar"; do
  echo "Updating $i";
  pushd ${i}
  git pull origin master
  popd
done

### Dev Env

This repository assumes you have the following in your current environment:

    - VirtualBox
    - Vagrant
      - Plugins:
        - vagrant-share
        - vagrant-hostmanager (Quality of life, auto-manage hosts file for guest inter-connectivity)
        - vagrant-cachier (Quality of life, caches packages locally)


## To start hacking the following should do the needful:

```
rawr@ds$ git clone --recursive git@git.drk.sc:darkscience/dev-env.git dev-env && cd $_
rawr@ds$ vagrant status
```


## How to keep this environment up to date with "prod"
Both darkscience/ds-salt and darkscience/fake-ds-pillar are contained as submodules in this repository meaning as
they receive updates the references for the submodules will need to be brought up to date as well. The following
workflow should suffice for these purposes:

```
rawr@ds$ ./update_repos.sh
rawr@ds$ git commit -m 'Updating pillar/state references to current'
rawr@ds$ git push origin master
```

## How to point this environment at your forks of ds-salt and fake-ds-pillar respectively
To remove the references to the upstream versions of these repositories and develop against a local
fork of them the following should suffice:

```
# Repoint ds-salt to your fork
rawr@ds$ pushd saltstack/salt
rawr@ds$ git remote rm origin
# Switch <username> with your username
rawr@ds$ git remote add origin git@git.drk.sc:<username>/ds-salt.git
rawr@ds$ popd

# Repoint fake-ds-pillar to your fork
rawr@ds$ pushd saltstack/pillar
rawr@ds$ git remote rm origin
# Switch <username> with your username
rawr@ds$ git remote add origin:<username>/fake-ds-pillar.git
rawr@ds$ popd
```


